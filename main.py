def writer_creator(file_name):
    def writer(line):
        with open(file_name, 'a') as file:
            file.write(line + '\n')
    return writer

def imprime_tudo(func):
    writer = writer_creator('args.log')
    
    def wrapper(*args, **kwargs):
        writer(str(args))
        return func(*args, **kwargs)
    return wrapper

@imprime_tudo
def soma(a, b):
    return a + b

soma(1,2)
soma(4,9)

writer = writer_creator('teste.log')
writer('oi1')
writer('oi2')
writer('oi3')